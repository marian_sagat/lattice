﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Experiment_1_Svazy
{
    public class Lattice
    {
        List<Node> listNodes;
        Node node_0;
        Node node_1;

        public Lattice()
        {
            listNodes = new List<Node>() { };
        }
        public Node findNode(string element)
        {

            return listNodes.Find(n => n.element == element);
        }

        public void insertNode(Node node)
        {
            // tu treba dat ze neexistuje uz v liste, kvoli jednoznacnosti
            listNodes.Add(node);
        }
        public void printListNodes()
        {
            Console.WriteLine("<<<<<<<<<<<<< LATTICE >>>>>>>>>>>>>>>>>>>>>>>");
            for (int i = 0; i < listNodes.Count; i++)
            {
                listNodes[i].print();
            }
        }
        public void initializeLattice(string orderedInequalityElementPairsDevidedBySpace)
        {
            var s = orderedInequalityElementPairsDevidedBySpace;
            //s.Split(new string[] { " " },StringSplitOptions.None).Where(x=>x!=" ").ToList();
            var s2 = s.Replace(" ", string.Empty);
            //Console.WriteLine(s2);
            //Console.WriteLine(s);
            List<char> orderedInequalityElementPairs = s.Replace(" ", string.Empty).ToList();
            List<char> distinctElements = orderedInequalityElementPairs.Distinct().ToList();
            Console.WriteLine("initialization Lattice:");
            distinctElements.ForEach(Console.Write);
            foreach (var item in distinctElements)
            {
                Console.WriteLine(item);
            }


            foreach (var item in distinctElements)
            {
                new Node(this, item.ToString());
            }

            Node node_lhs,node_rhs;//node_lhs < node_rhs
            for (int i = 0; i < orderedInequalityElementPairs.Count/2; i++)
            {
               node_lhs= findNode(orderedInequalityElementPairs[2 * i].ToString());
               node_rhs = findNode(orderedInequalityElementPairs[2 * i+1].ToString());
                node_lhs.addNext(node_rhs);
            }


            /// UPGRADE NODE 0 OR CREATE NODE 0
            node_0 = findNode("0");
            if (node_0==null)
            {
                node_0 = new Node(this, "0");
            }

            foreach (var node in listNodes)
            {
                if (node.element!="0" && node.previous==null)
                {
                    node_0.addNext(node.element);
                }
            }

            /// UPGRADE NODE 1 OR CREATE NODE 1
            node_1 = findNode("1");
            if (node_1==null)
            {
                node_1 = new Node(this, "1");
            }

            foreach (var node in listNodes)
            {
                if (node.element != "1" && node.next == null)
                {
                    node_1.addPrevious(node.element);
                }
            }

        }
        public void draw_Lattice()
        {
            //create a form 
            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            //create a viewer object 
            Microsoft.Msagl.GraphViewerGdi.GViewer viewer = new Microsoft.Msagl.GraphViewerGdi.GViewer();
            //create a graph object 
            Microsoft.Msagl.Drawing.Graph graph = new Microsoft.Msagl.Drawing.Graph("graph");
            graph.Directed = false;
            //create the graph content 
            foreach (var node in listNodes)
            {
                if (node.next!=null)
                {
                    foreach (var item in node.next)
                    {
                        graph.AddEdge(node.element, item);
                    }
                }
            }

            //graph.AddEdge("A", "C");
            
            //graph.FindNode("A").Attr.FillColor = Microsoft.Msagl.Drawing.Color.Magenta;
            //graph.FindNode("B").Attr.FillColor = Microsoft.Msagl.Drawing.Color.MistyRose;
            //Microsoft.Msagl.Drawing.Node c = graph.FindNode("C");
            //c.Attr.FillColor = Microsoft.Msagl.Drawing.Color.PaleGreen;
            //c.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Circle;
            foreach (var node in graph.Nodes)
            {
                node.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Circle;
            }
            //bind the graph to the viewer 
            viewer.Graph = graph;

            //associate the viewer with the form 
            form.SuspendLayout();
            viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            form.Controls.Add(viewer);
            form.ResumeLayout();
            //show the form 
            form.ShowDialog();
        }

    }
}
