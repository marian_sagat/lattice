﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Experiment_1_Svazy
{
    public class Node
    {
        public string element;
        public List<string> next;
        public List<string> previous;
        Lattice lattice;

        public Node()
        {
            element = "";
            next = null;
            previous = null;
            lattice = null;
        }
        public Node(Lattice lattice, string element)
        {
            this.element = element;
            this.lattice = lattice;
            var node = lattice.findNode(element);
            if (node == null)//neexistuje node, vytvor ho
            {
                lattice.insertNode(this);
                
                previous = new List<string>() { };
            }
            next = null;
            previous = null;
            
        }
        public void print()
        {
            Console.WriteLine("*** e= " + element + "***");
            if (next == null)
            {
                Console.Write("Next: NULL");
            }
            else 
            {
                Console.Write("Next: ");
                for (int i = 0; i < next.Count; i++)
                {
                    Console.Write(next[i] + " ");
                }
                if (next.Count==0)
                {
                    Console.Write("empty");
                }
            }

            Console.WriteLine();

            if (previous == null)
            {
                Console.Write("Previous: NULL");
            }
            else
            {

                Console.Write("Previous: ");
                for (int i = 0; i < previous.Count; i++)
                {
                    Console.Write(previous[i] + " ");
                }
                if (previous.Count == 0)
                {
                    Console.Write("empty");
                }
            }

            Console.WriteLine();
            Console.WriteLine();

        }
        public void addNext(string nextElement)
        {
            if (next==null)
            {
                next = new List<string>() { };
            }
            next.Add(nextElement);
            var node = lattice.findNode(nextElement);
            if (node == null)//neexistuje, vytvor ho
            {

                lattice.insertNode(new Node(lattice, nextElement));
            }
            else
            {
                if (node.findPrevious(element) == null)
                {
                    node.addPrevious(element);
                }

            }
        }
        public void addNext(Node nextNode)
        {
            if (nextNode == null)
            {
                Console.WriteLine("node doesn't exist!");
                return;
            }

            if (next == null)
            {
                next = new List<string>() { };
            }

            var nextElement = nextNode.element;
            next.Add(nextElement);
            var node = lattice.findNode(nextElement);
            if (node == null)//neexistuje, vytvor ho
            {

                lattice.insertNode(new Node(lattice, nextElement));
            }
            else
            {
                if (node.findPrevious(element) == null)
                {
                    node.addPrevious(element);
                }

            }
        }

        public void addPrevious(string previousElement)
        {
            if (previous==null)
            {
                previous = new List<string>() { };
            }
            previous.Add(previousElement);
            var node = lattice.findNode(previousElement);
            if (node == null)
            {
                lattice.insertNode(new Node(lattice, previousElement));
            }
            else
            {
                if (node.findNext(element) == null)
                {
                    node.addNext(element);
                }

            }
        }
        public void addPrevious(Node previousNode)
        {
            if (previousNode == null)
            {
                Console.WriteLine("node doesn't exist!");
                return;
            }

            if (previous == null)
            {
                previous = new List<string>() { };
            }

            var previousElement = previousNode.element;
            previous.Add(previousElement);
            var node = lattice.findNode(previousElement);
            if (node == null)
            {
                lattice.insertNode(new Node(lattice, previousElement));
            }
            else
            {
                if (node.findNext(element) == null)
                {
                    node.addNext(element);
                }

            }
        }
        public string findNext(string nextElement)
        {
            if (next==null)
            {
                return null;
            }
            return next.Find(s => s == nextElement);
        }
        public string findPrevious(string previousElement)
        {
            if (previous==null)
            {
                return null;
            }
            return previous.Find(s => s == previousElement);
        }
    }
}
