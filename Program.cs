﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Experiment_1_Svazy
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Lattice lattice = new Lattice();
            Node a = new Node(lattice, "a");
            Node b = new Node(lattice, "b");
            Node c = new Node(lattice, "c");
            lattice.printListNodes();

            // a<b
            a.addNext(b);
            //a<c
            a.addNext(c.element);



            lattice.printListNodes();



            Lattice lattice2 = new Lattice();
            // a<b = ab
            string s = "ab ac ad be ce df ef gb dh 0i";

            lattice2.initializeLattice(s);
            Console.WriteLine("*************- zobraz lattice2 *************");
            lattice2.printListNodes();


            Lattice lattice3 = new Lattice();
            string s3 = "ab 0c de ef";
            lattice3.initializeLattice(s3);
            Console.WriteLine("************ zobraz lattice3 *************");
            lattice3.printListNodes();
            lattice3.draw_Lattice();

            //test_MS_Automated_Graph_Layout();

            Console.ReadLine();
        }
        static void test_MS_Automated_Graph_Layout()
        {
            //create a form 
            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            //create a viewer object 
            Microsoft.Msagl.GraphViewerGdi.GViewer viewer = new Microsoft.Msagl.GraphViewerGdi.GViewer();
            //create a graph object 
            Microsoft.Msagl.Drawing.Graph graph = new Microsoft.Msagl.Drawing.Graph("graph");
            graph.Directed = false;
            //create the graph content 
            string s="ab ac ad be ce df ef gb dh 0i";
            graph.AddEdge("A", "B");
            graph.AddEdge("A", "C");
            graph.AddEdge("A", "D").Attr.Color = Microsoft.Msagl.Drawing.Color.Green;
            graph.AddEdge("B", "E");
            graph.AddEdge("C", "E");
            graph.AddEdge("D", "F");
            graph.AddEdge("E", "F");
            graph.AddEdge("G", "B");
            graph.AddEdge("D", "H");
            graph.AddEdge("0", "i");
            //graph.FindNode("A").Attr.FillColor = Microsoft.Msagl.Drawing.Color.Magenta;
            //graph.FindNode("B").Attr.FillColor = Microsoft.Msagl.Drawing.Color.MistyRose;
            Microsoft.Msagl.Drawing.Node c = graph.FindNode("C");
            c.Attr.FillColor = Microsoft.Msagl.Drawing.Color.PaleGreen;
            c.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Circle;
            foreach (var node in graph.Nodes)
            {
                node.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Circle;
            }
            //bind the graph to the viewer 
            viewer.Graph = graph;

            //associate the viewer with the form 
            form.SuspendLayout();
            viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            form.Controls.Add(viewer);
            form.ResumeLayout();
            //show the form 
            form.ShowDialog();
        }
    }
}
